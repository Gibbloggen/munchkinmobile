/*--------------------------------------------------------------
 Program:      Munchkin Mobile
 
 Description:  An Arduino System tha controls a wheeled robot's
 Electronics.  Inclusive of Blinkers w/sound,
 Headlights, a horn, and an LCD display, controlled
 from a web browser that sends out a web page
 
 Hardware:     - Arduino Uno and an Arduino Ethernet
 shield. 4 red LED's and 2 Green/White,
 3 - 220 ohm resistors.  And LCD screen 16x2
 with backpack. A speaker, and various wires and a bread board.
 
 Software:     Developed using Arduino 1.0.5 software
 
 License:      This software is Public Domain.
 
 
 References:   
 -Tyler Cooper's AdaFruit.com LCD tutorial
 https://learn.adafruit.com/usb-plus-serial-backpack
 Which is also where I bought the LCD
 
 -W.A. Smith, http://startingelectronics.com/tutorials/arduino/ethernet-shield-web-server-tutorial/web-server-LED-control/
 - WebServer example by David A. Mellis and 
 modified by Tom Igoe
 - Ethernet library documentation:
 http://arduino.cc/en/Reference/Ethernet
 - Most of the wiring was extrapolated from arduino.cc's web site lessons, and their examples.
 
 Date:         March, 19th, 2015
 
 Author:       John Leone  Gibbloggen@gmail.com
 --------------------------------------------------------------*/

#include <SPI.h>
#include <Ethernet.h>
#include <SoftwareSerial.h>

//Mac address is arbitrary.  I am using an older network shield that doesn't 
//come with one.
byte mac[] = {
  0xDF, 0xBD, 0xFF, 0xEE, 0xDD, 0xAB};
  
IPAddress ip(192, 168, 1,118 );// This ip I made up tha works well with my
//router.

//80 is the standard web server port
EthernetServer server(80);  // create a server at port 80

String HTTP_req;          // stores the HTTP request
//boolean LED_status = 0;   // state of LED, off by default

//Paste from Network Controlled Telnet prototype...


//int led = 0; // an indexing variable to traverse the array

boolean rightFlash = false;
boolean leftFlash = false;
boolean gotLights = false;

long flashFrequency = 500;

unsigned long lastFlash = 0;


static int brightness = 0;    // an inexing variable

int ledChooser[5] = {
  2,3,5 ,6,9}; // An array of the digital pins for led's


boolean first = true;  //This controls "GET" response parsing from being called too often.


boolean alreadyConnected = false; // whether or not the client was connected previously


//This is the serial connection to connect to the lcd
SoftwareSerial lcd = SoftwareSerial(0,9);


void setup()
{

  //This is reserving space, so that the string does not get 
  //overwritte, the 350 may have to be trimmed.  This
  //combined with using the F() command on constant strings
  //in commands has freed up enough memory for that.
  //Thanks go to adafruit_support_rick a forum moderator of 
  //Adafruit for these memory tips.
  HTTP_req.reserve(350);
  //int led = 0; // an indexing variable to traverse the array
  for (int led=0; led<5; led++) 
    pinMode(ledChooser[led], OUTPUT );

  // End of init pins

  Ethernet.begin(mac, ip);  // initialize Ethernet device
  server.begin();           // start to listen for clients
  Serial.begin(9600);       // for diagnostics, this goes to the serial
  //monitor

  //LCD Code Here
  lcd.begin(9600);
  delay(10); //these delays are important.
  //I am keeping them in production in 
  //setup, but am going to try and figure something else
  //out for the parts that run in the loop call
  //  Serial.println("In GoLCD");
  ////
  //
  // // set the size of the display if it isn't 16x2 (you only have to do this once)
  lcd.write(0xFE);
  lcd.write(0xD1);
  lcd.write(16);  // 16 columns
  lcd.write(2);   // 2 rows
  delay(10);       

  // This is the green background.. 
  uint8_t white = 255;
  lcd.write(0xFE);
  lcd.write(0xD0);
  lcd.write(0x10); 
  lcd.write(0x10);
  lcd.write(0x10);
  delay(10);  // give it some time to adjust the backlight!
  ////// set the contrast, 200 is a good place to start, adjust as desired
  lcd.write(0xFE);
  lcd.write(0x50);
  lcd.write(200);
  delay(10);       
  ////  
  ////  // set the brightness - we'll max it (255 is max brightness)
  lcd.write(0xFE);
  lcd.write(0x99);
  lcd.write(255);
  delay(10);       
  ////  
  ////  // turn off cursors
  lcd.write(0xFE);
  lcd.write(0x4B);
  lcd.write(0xFE);
  lcd.write(0x54);
  delay(10);
  ////// go 'home'
  lcd.write(0xFE);
  lcd.write(0x48);
  delay(10);   // we suggest putting delays after each command 
  // End LCD Initialization code


}

void loop()
{

  //This is the routine call that controls the blinkers
  //Flashing as well as their sound.
  CheckFlash();


  EthernetClient client = server.available();  // try to get client

  if (client) {  // got client?
    boolean currentLineIsBlank = true;
    while (client.connected()) {
      if (client.available()) {   // client data available to read
        char c = client.read(); // read 1 byte (character) from client
        HTTP_req += c;  // save the HTTP request 1 char at a time
        // last line of client request is blank and ends with \n
        // respond to client only after last line received
        if (c == '\n' && currentLineIsBlank) {

          //Notice in all these strings the use of the F(), this releases it from
          //memory, so we don't overflow the sram

          // send a standard http response header
          client.println(F("HTTP/1.1 200 OK"));
          client.println(F("Content-Type: text/html"));
          client.println(F("Connection: close"));
          client.println();
          // send web page
          client.println(F("<!DOCTYPE html>"));
          client.println(F("<html>"));
          client.println(F("<head>"));
          client.println(F("<title>Arduino Wheeled Robot</title>"));
          client.println(F("</head>"));

          //The java script "offhorn" just turns off the horn after it beeps
          client.println(F("<body onload=\"offhorn();\" style=\"width:100%;\">"));

          // Here is some simple div formatting.  Need to research how I can control screen
          // sizes for mobile devices.
          client.println(F("<div style=\"text-align:center;color:blue;float:left\">"));

          client.println(F("<h1>Control from any device with a web browser</h1>"));
          client.println(F("<p>To Make the robot go, left, right, forward, turn on the headlights and beep the horn</p>"));
          client.println(F("<form method=\"get\">"));

          //This is the routine that outputs the form contents
          ProcessCheckbox(client);

          client.println(F("</form>"));

          client.println(F("</body>"));
          client.println(F("</html>"));
          //Serial.print(HTTP_req);
          HTTP_req = "";    // finished with request, empty string
          break;
        }
        // every line of text received from the client ends with \r\n
        if (c == '\n') {
          // last character on line of received text
          // starting new line with next character read
          currentLineIsBlank = true;
        } 
        else if (c != '\r') {
          // a text character was received from client
          currentLineIsBlank = false;
        }
      } // end if (client.available())
    } // end while (client.connected())
    delay(1);      // give the web browser time to receive the data
    client.stop(); // close the connection
  } // end if (client)
}

// switch LED and send back HTML for LED checkbox
void ProcessCheckbox(EthernetClient cl)
{

  //This is where the global first comes in, It just makes sure that
  //it doesn't call this in too large of a loop, because if it does, it kills the processor.
  if(first){ 
    first = false;

    //  Simple Diagnostics, it let's me see what it is trying to parse
    Serial.println(F("\nHere is the request...:"));
    Serial.println(HTTP_req);
    Serial.println(F(":End of request\n"));
    //  Serial.println();

    //these are simple char arrays that I 
    //populate with "checked" if the radio button should be populated.

    char rChecked[8] = "";
    char leChecked[8] = "";
    char fChecked[8] = "";
    char bChecked[8] = "";


    //This is another call to the CheckFlash command.  For processing
    //blinker flash and sound
    CheckFlash();


    //This loops through the top line of the "GET" request, it
    //ignores field name, for now, and I keep each value unique.
    //so that I just have a switch on the value.
    for (int i= 0; i<30;i++){
      //Serial.println(HTTP_req[i]);
      if(HTTP_req[i] == 61){
        // Serial.print(F("Should be Equals?"));
        // Serial.println(HTTP_req[i]);
        i++;
        switch(HTTP_req[i]){

        case 'R':
          Serial.println(F("Got Right"));
          strcpy(rChecked, "checked");
          rightFlash = true;
          leftFlash = false;
          break;

        case 'F':
          Serial.println(F("Got Forward"));
          
          strcpy(fChecked, "checked");    
          rightFlash = false;
          leftFlash = false;
          break;

        case 'L':
          Serial.println(F("Got Left"));
          strcpy(leChecked, "checked");
          rightFlash = false;
          leftFlash = true;
          break;

        case '1':
          strcpy(bChecked, "checked");
          gotLights = true;
          digitalWrite(5, 1);
          break;

        case 'H':
          tone(6, 265, 1000);
          break;

        default:
          Serial.print(F("We Got something weird in the case"));
          Serial.println(HTTP_req[i]);
          break;

        }
      }
    }
    //This turns off the headlights.  The green led's
    if(bChecked[0] != 'c' )
      digitalWrite(5, 0);

    //This is the call to the routine that updates the LCD
    //I have a license plate in the top line, and a message
    //of either going right or going left, or just crusiing for forward.

    GoLCD();


    cl.print(F("<p><input type=\"radio\" id=\"BLINKER3\" name=\"BLK\" value=\"L\" "));
    cl.print( leChecked);
    cl.println(F( ">LEFT "));

    cl.print(F("<input type=\"radio\" id=\"BLINKER2\" name=\"BLK\" value=\"F\" "));
    cl.print( fChecked);
    cl.println(F(">FORWARD"));

    cl.print(F("<input type=\"radio\" id=\"BLINKER\" name=\"BLK\" value=\"R\" "));
    cl.print( rChecked);
    cl.println(F(">RIGHT</p>"));


    cl.print(F("<p><input type=\"checkbox\" id=\"HEADLIGHTS\" name=\"LTS\" value=\"1\" "));
    cl.print( bChecked);
    cl.println(F( ">HEADLIGHTS </p>"));

    cl.println(F("<p style=\"text-align:center;\"><input type=\"submit\" value=\"make changes\" > </p>"));


    cl.print(F("<p style=\"text-align:center;\"> <input type=\"text\" style=\"color:white;border-width:0px;;\" id=\"horn\" name=\"HRN\" value=\"\"> </p>"));
    cl.print (F("<p style=\"text-align:center;\"><button onclick=\"beephorn()\">Beep Horn</button></p>"));

    //two simple scripts that control the horn.
    cl.print (F("<script> function beephorn() { document.getElementById(\"horn\").value = \"H\"; } </script>"));
    cl.print (F("<script> function offhorn() { document.getElementById(\"horn\").value = \"\"; } </script>"));





  } 
  else first = true; // if it passed it up once, next time around it will run it again.
  //kinda hackish, but it works:-)

}


//This is the checkflash.  the code was inspired by the multi tasking w/out delay()
//The purpose of this is that a delay freezes the code, where as these ticker
//checks are single pass and then keep going.

void CheckFlash()
{
  unsigned long currentMillis = millis();
  if((currentMillis - lastFlash) < ( 2 * flashFrequency))
  {
    if (( currentMillis - lastFlash) >  flashFrequency)
    {
      if(leftFlash){
        //This is the right blinker on, left blinker off
        //and the tone for the blinker
        digitalWrite(3,1);
        digitalWrite(2,0);
        tone(6, 2000, 53);
        //       
      }
      else if(rightFlash){
        //This is the right blinker on, left blinker off
        //and the tone for the blinker
        digitalWrite(2,1);
        digitalWrite(3,0);
        tone(6, 2000, 53);
      }
      else{
        //this is forward, so there are no blinkers
        digitalWrite(3,0);
        digitalWrite(2,0);
      }
    }
    else
    {  //If it's not time to blink, it shuts off the lights.
      //Thusly it blinks :-)
      digitalWrite(3,0);
      digitalWrite(2,0);
    }


  } 
  else lastFlash = currentMillis;


}
void GoLCD(){

  //clear screen
  lcd.write(0xFE);
  lcd.write(0x58);
  delay(10);   // we suggest putting delays after each command 

  ///// go 'home'
  lcd.write(0xFE);
  lcd.write(0x48);
  delay(10);   // we suggest putting delays after each command 

  lcd.println(F("MUNCHKIN MOBILE"));  //this is the topline, her licenseplate..

  //This is a custom message to describe where the car is signaling too.
  //I'm hoping somewhat similar code will motion the robot
  if( leftFlash ) lcd.print(F("Going LEFT"));
  else if ( rightFlash ) lcd.print(F("Going Right"));
  else lcd.print(F("Just Cruising."));
  delay(10);
}




