﻿Munchkin Mobile README

This is a project where I am programming an Arduino board to: display
blinkers, with sound; headlights; horn; and an LCD screen.

Thus far I have a working prototypes and I make demo's on youtube 
at... https://www.youtube.com/user/RadiolistenerGoesVid.

I am making this source code public, so that I can share with others 
my progress.  Instead of pasting it on help forums, or to show source 
with a video, I'll be able to just put a link to this repository.

The source is  Public Domain, so do with it what you'd like.  If you 
want to reach me regarding any of this, my email is Gibbloggen@gmail.com

Thanks for stopping by,

John Leone.


